## About
This is an attempt to implement
[cellular automata like Stephen Wolfram does](http://mathworld.wolfram.com/CellularAutomaton.html)

Unfortunately, I have not yet read a single page of his seemingly
[awesome book](http://www.wolframscience.com/nksonline/toc.html),
but took my information solely from
[this YouTube video](https://www.youtube.com/watch?v=_eC14GonZnU).

## Requirements
- [Pillow: the friendly PIL fork](https://python-pillow.github.io/)

## Usage
Every tweakable setting is in `settings.py`, so there shouldn't be
any need to actually fumble with the code itself. Have a look at the
settings, adjust `settings.py` to your likings and run:

    python cellauto.py

