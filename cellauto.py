#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Automaton
import settings


def generate_all(from_=0, to_=255):
    """Generate images in a given size for all rules.
    Uses generate_one().
    """
    # for each ruleset
    for i in xrange(from_, to_ + 1):
        # generate the image
        generate_one(i)
    return

def generate_one(ruleno=None):
    """Generate an image in a given size for a single given rule."""
    # which rule should we use
    if ruleno is None:
        ruleno = settings.ruleno

    # create automaton with a width and a ruleno
    a = Automaton.Automaton(settings.width, ruleno)

    # generate an image with a given height
    a.generate_img(
        height=settings.height,
        ftype=settings.ftype,
        mark_duplicates=settings.mark_duplicates
    )
    return


if __name__ == "__main__":
    if 0 < settings.ruleno < 256:
        generate_one(settings.ruleno)
    else:
        generate_all()
