#!/usr/bin/env python
# -*- coding: utf-8 -*-

import codecs
import itertools
import sys


class RuleGenerator(object):
    """Generate all possible rules for the cellular
    automaton that include n determining cells.
    """
    def __init__(self):
        self.rulesets = self.__generate_rulesets()

    def __generate_rulesets(self):
        """Produces rules up to depth n.
        """
        rulesets = []

        # generate initial, simplest rules
        baserules = [r for r in itertools.product("01", repeat=3)]
        baserules = list(reversed(baserules))

        # for all possible combinations
        for i in range(256):
            # create a binary representation of which rule should be active
            rule = "{0:b}".format(i).zfill(2 ** 3)
            tmp = []
            # save the rule to a new ruleset if necessary
            for i in range(8):
                if rule[i] == "1":
                    tmp.append(baserules[i])
            # append the new ruleset to rulesets
            rulesets.append(tmp)
        return rulesets


if __name__ == "__main__":
    rg = RuleGenerator()
