#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import settings
import sys

import RuleGenerator

from PIL import Image


class Automaton():
    """A cellular automaton.
    """

    def __init__(self, width, ruleno, row=None):
        self.width = width
        if row is None:
            self._reset()
        else:
            self.row = row

        self.ruleno = ruleno
        self.ruleset = self.__get_ruleset(ruleno)
        self.rows = []
        self.hashes = []
        self.duplicates = []
        return

    def __get_ruleset(self, ruleno):
        """Get the ruleset corresponding to the rule number.
        Uses the RuleGenerator.
        """
        rg = RuleGenerator.RuleGenerator()
        rules = rg.rulesets[ruleno]

        # convert strings to int
        for i in range(len(rules)):
            rules[i] = [int(j) for j in rules[i]]
        return rules

    def __iter__(self):
        return self

    def next(self, stop_on_duplicate, mark_duplicates):
        # save the last iteration
        lastrow = self.row
        # apply rule to last iteration and update current iteration
        self.row = self._apply(self.row)

        # if the row was generated before, we have found a loop
        if (mark_duplicates is True or stop_on_duplicate is True) \
                and hash(tuple(self.row)) in self.hashes:

            # remember the indices of the duplicate rows
            if mark_duplicates is True:
                self.duplicates.append(self.rows.index(self.row))
                self.duplicates.append(len(self.rows))

            if stop_on_duplicate is True:
                raise StopIteration

        return self.row

    def generate_img(self, height=-1, ftype="PNG", mark_duplicates=False):
        """Save image file to disk.
        NOTE: will reset the generator!
        """
        # if height is not passed in
        if height <= 0:
            # continue until we get a duplicate
            stop_on_duplicate = True
            print(
                "##########\n"
                "Generating image until an iteration reappears. "
                "BE AWARE that this might be an image of\n\n{}\n\npixels in height.\n"
            ).format(
                2 ** self.width
            )
        else:
            print("Generating image of dimensions {}x{}.").format(
                self.width,
                height
            )
            stop_on_duplicate = False

        self._reset()

        try:
            l = 0
            # for every line
            while True:

                # assert that we don't exceed image height
                if stop_on_duplicate is False and l == height:
                    break
                # don't overdo it - stop at iteration n
                elif stop_on_duplicate is True and l > settings.maxheight:
                    break

                # all possible rows have been drawn
                if l > 2 ** self.width:
                    raise Exception

                self.rows.append(self.row)
                self.hashes.append(hash(tuple(self.row)))

                # advance one iteration
                self.row = self.next(
                    stop_on_duplicate=stop_on_duplicate,
                    mark_duplicates=mark_duplicates
                )

                l = l + 1

            self._render_img_to_disk(ftype=ftype)
            return

        except StopIteration:
            # todo: color in the rest of the image with the last row,
            # as this is what would actually happen if iteration continued.

            # generate the duplicate line manually and append
            self.rows.append(self._apply(self.rows[-1]))
            self.hashes.append(hash(tuple(self.row)))
            self._render_img_to_disk(ftype=ftype)
            return

        except KeyboardInterrupt:
            self._render_img_to_disk(ftype=ftype)
            # and pass on the Interrupt
            # strangely, this doesn't always work as expected
            raise
        return

    def _render_img_to_disk(self, ftype="BMP"):
        """Render an image of self.rows to disk.
        """
        print(
            "Rendering image of size {}x{} to disk. Please "
            "be patient as this might take a while"
        ).format(
            self.width,
            len(self.rows)
        )

        # create a new image
        if ftype == "TXT":
            fname = "R{}.{}x{}.{}".format(
                str(self.ruleno).zfill(3),
                self.width,
                len(self.rows),
                ftype.lower()
            )
            with open(fname, "w") as f:
                for row in self.rows:
                    f.write("".join([str(cell) for cell in row]) + "\n")
            return
        elif ftype == "BMP":
            img = Image.new("1", (self.width, len(self.rows)), "white")
        elif ftype == "PNG":
            img = Image.new("RGB", (self.width, len(self.rows)), "white")


        # load its pixels
        pixels = img.load()

        # for every row
        for i in range(len(self.rows)):
            # for every pixel
            for j in range(self.width):
                # color in the pixels accordingly
                if ftype == "BMP":
                    pixels[j, i] = int(not self.rows[i][j])
                elif ftype == "PNG":
                    if self.rows[i][j] == 0:
                        if i in self.duplicates:
                            pixels[j, i] = (255, 0, 0)
                        else:
                            pixels[j, i] = (255, 255, 255)
                    else:
                        pixels[j, i] = (0, 0, 0)

        # write image to disk
        img.save(
            "R{}.{}x{}.{}".format(
                str(self.ruleno).zfill(3),
                self.width,
                len(self.rows),
                ftype
            ),
            ftype
        )
        return

    def _reset(self):
        self.row = [0 for i in range(self.width)]
        self.row[self.width / 2] = 1
        self.rows = []
        return

    def _apply(self, row):
        """Applies rules to a given row and
        returns the next row aka iteration.
        """
        self.row = row
        return [self._is_alive(i) for i in range(self.width)]

    def _is_alive(self, i):
        """Determines if a cell should be alive given
        its position in the current row.
        """
        # TODO: make this not care about rulesize

        # create a triplet from the previous row[i-1:i+2]
        if 0 < i < len(self.row) - 1:
            triplet = self.row[i - 1:i + 2]
        elif i == 0:
            triplet = [0] + self.row[i:i + 2]
        else:
            triplet = self.row[i - 1:i + 1] + [0]

        # if the triplet matches a rule
        if triplet in self.ruleset:
            # this cell should be alive
            return 1
        # this cell should be dead
        return 0
