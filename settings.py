#!/usr/bin/env python
# -*- coding: utf-8 -*-

#####
# Settings for Automaton

# Which rule to use. Set to negative to
# generate an image for each possible rule.
ruleno = -30

# rules that are not totally boring
goodrules = [
    18,
    22,
    57,
    60,
    110,
    124,
    126,
    30,
    45,
    73,
    105,
    109,
    129,
    150,
    161,
    165,
    151,
    169,
]

# Automaton/image width
width = 31
# Max height of automaton and image. Set negative for continued generation
# that will only stop on a repeating row. This might in principal generate
# images with a height up to 2**width images, so be careful.
height = width

# where to stop continuous generation
maxheight = 10000

# initial white row...
initrow = [0 for i in range(width)]
# ...with a black pixel in the center
initrow[width / 2] = 1

# The type of file to generate. Currently available are "BMP", "PNG".
# Note that "BMP" will not be able to highlight duplicate rows.
ftype = "PNG"  # "BMP"

# Whether to mark the duplicate lines in the file
mark_duplicates = True
